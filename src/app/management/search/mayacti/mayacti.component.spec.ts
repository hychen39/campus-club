import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MayactiComponent } from './mayacti.component';

describe('MayactiComponent', () => {
  let component: MayactiComponent;
  let fixture: ComponentFixture<MayactiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MayactiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MayactiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
