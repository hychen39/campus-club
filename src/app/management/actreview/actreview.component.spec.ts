import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActreviewComponent } from './actreview.component';

describe('ActreviewComponent', () => {
  let component: ActreviewComponent;
  let fixture: ComponentFixture<ActreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
