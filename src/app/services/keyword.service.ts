import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Activity} from '../models/activity';
import {HttpClient} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {OrdsResponse} from '../models/ords-response';
import {HttpParams} from '@angular/common/http';


/**
 * 和表格的欄位名稱一致, 用以解析回傳結果.
 * 回傳的 json 欄位使用小寫
 */
export interface TicketTable {
  id: number;
  title: string;
  venue: string;
  intro: string;
  start_date: string;
  end_date: string;
  act_date: string;
  img: string;
}

@Injectable({
  providedIn: 'root'
})
export class KeywordService {
  // Done: start from here 2021/8/24
  private TICKET_URI = 'http://163.17.9.165/ords/app107_17/club/keyword';

  // 存放查詢回來的結果，供其它元件使用。
  public queriedActivities: Activity[];

  constructor(private httpClient: HttpClient) {
  }

  // 增加輸入參數 title
  public fetchKeyword(title: string) {

    // 改成輸入參數
    // const title =[''];
    const filterObject = `{"TITLE": {"$like": "%${title}%"}}`;
    const queryParam: HttpParams = new HttpParams().set('q', filterObject);

    let activities: Activity [] = [];
    return this.httpClient.get(this.TICKET_URI, {params: queryParam})
      .pipe(
        // tap((response: OrdsResponse<TicketTable>) =>
        // {response.items.forEach(
        //   (value => console.log(value))
        // ); }),
        map((response: OrdsResponse<TicketTable>) => {
          activities = response.items.map((ticket: TicketTable) => new Activity(
              ticket.title,
              new Date(ticket.start_date),
              new Date(ticket.end_date),
              ticket.venue,
              ticket.intro,
              ticket.img
            )
          );
          return activities;
        }),
        catchError(error => of('Caught: ${error}'))
      );
  }
}

