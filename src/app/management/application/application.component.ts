import { Component, OnInit } from '@angular/core';
import {Application} from '../../models/application';
import {ApplicationService} from '../../services/application.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  activityApplication: Application;

  constructor(private applicationService: ApplicationService) {
    this.activityApplication = new Application();
  }

  ngOnInit(): void {
    this.activityApplication = this.applicationService.genTestActivityAppData();
  }

  public submit(){
    this.applicationService.submit(this.activityApplication)
      .subscribe((next) => console.log('Response for the submission', next));
  }
}
