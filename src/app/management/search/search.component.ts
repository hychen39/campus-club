import { Component, OnInit } from '@angular/core';
import {ActivityService} from '../../services/acivity.service';
import {Activity} from '../../models/activity';


@Component({ 
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  activities: Activity[] | undefined;
  constructor(private activityService: ActivityService) {
  }
  
  ngOnInit(): void {
    this.activities = this.activityService.findActivities();
  }

}

