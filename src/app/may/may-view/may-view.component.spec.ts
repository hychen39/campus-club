import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MayViewComponent } from './may-view.component';

describe('MayViewComponent', () => {
  let component: MayViewComponent;
  let fixture: ComponentFixture<MayViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MayViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MayViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
