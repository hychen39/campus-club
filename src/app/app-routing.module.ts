import { TicketViewComponent } from './tickets/ticket-view/ticket-view.component';
import { ActivityViewComponent } from './activities/activity-view/activity-view.component';
import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryViewComponent } from './category/category-view/category-view.component';
import { InterestViewComponent } from './interest/interest-view/interest-view.component';
import { MayViewComponent } from './may/may-view/may-view.component';
import { MessageViewComponent } from './message/message-view/message-view.component';
import { ManagementComponent } from './management/management.component';
import { ApplicationComponent } from './management/application/application.component';
import { EventsComponent } from './management/events/events.component';
import { CyutComponent } from './management/events/cyut/cyut.component';
import { SearchComponent } from './management/search/search.component';
import { MayactiComponent } from './management/search/mayacti/mayacti.component';
import { AccComponent } from './management/acc/acc.component';
import { ActreviewComponent } from './management/actreview/actreview.component';
import { ModiComponent } from './management/actreview/modi/modi.component';
import { PublishComponent } from './management/publish/publish.component';
import { KeywordComponent } from './keyword/keyword.component';


const routes: Routes = [
{path: 'activities', component: ActivityViewComponent},
{path: 'tickets', component: TicketViewComponent},
{path: 'category', component: CategoryViewComponent},
{path: 'interest', component: InterestViewComponent},
  // 加入 path parameter 以取得點選的活動編號
{path: 'may/:id', component: MayViewComponent},
{path: 'message', component: MessageViewComponent},
{path: 'management', component: ManagementComponent},
{path: 'application', component: ApplicationComponent},
{path: 'events', component: EventsComponent},
{path: 'cyut', component: CyutComponent},
{path: 'search', component: SearchComponent},
{path: 'mayacti', component: MayactiComponent},
{path: 'acc', component: AccComponent},
{path: 'actview', component: ActreviewComponent},
{path: 'modi', component: ModiComponent},
{path: 'publish', component: PublishComponent},
{path: 'keyword', component: KeywordComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
