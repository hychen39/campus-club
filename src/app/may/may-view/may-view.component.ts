import { Component, OnInit } from '@angular/core';
import {May} from '../../models/may';
import {MayService} from '../../services/may.service';
import {tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-may-view',
  templateUrl: './may-view.component.html',
  styleUrls: ['./may-view.component.css']
})
export class MayViewComponent implements OnInit {
  activities: May[] | string;
  // 注入 MayService 取得活動。這個名稱不是很好，建議改為 ActivityService
  // 注入 ActivatedRoute 取得目前的路徑資訊
  constructor(private mayService: MayService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    // 取得目前的路徑參數
    const id = Number.parseFloat(this.activatedRoute.snapshot.paramMap.get('id'));
    // 參數化 fetchTicket(), 傳入要取得的活動編號。
    this.mayService.fetchTicket(id)
      // .pipe(tap((value) => console.log(value)))
      .subscribe(
      (value => this.activities = value),
      ((error => {console.log(error); }))
    );
  }

}
