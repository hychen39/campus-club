import { Component, OnInit } from '@angular/core';
import {Activity} from '../models/activity';
import {KeywordService} from '../services/keyword.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-keyword',
  templateUrl: './keyword.component.html',
  styleUrls: ['./keyword.component.css']
})

export class KeywordComponent implements OnInit {
  activities: Activity[];
  private keywords: string;
  // Inject ActivatedRoute Service
  constructor(private keywordService: KeywordService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    // get the query parameter
    this.activatedRoute.queryParams.subscribe(
      queryParams =>  {
        this.keywords = queryParams.keywords;
        this.keywordService.fetchKeyword(this.keywords).subscribe(
          (returnedActivities) => {
            if (returnedActivities instanceof String) {
              this.activities = [new Activity( returnedActivities as string, null, null, null, null, null)];
            } else if ( (returnedActivities as Activity[]).length === 0) {
              this.activities = [new Activity( 'no result', null, null, null, null, null)];
            }
            else {
              this.activities = returnedActivities as Activity[];
            }
          }
        );
      }
    );
  }


}
