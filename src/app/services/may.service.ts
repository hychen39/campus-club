import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {May} from '../models/may';
import {HttpClient} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import {OrdsResponse} from '../models/ords-response';


/**
 * 和表格的欄位名稱一致, 用以解析回傳結果.
 * 回傳的 json 欄位使用小寫
 */
export interface MayTable {
  id: number;
  title: string;
  venue: string;
  intro: string;
  start_date: string;
  // interface 欄位名稱與 json 欄位名稱不一致。 act_Date 是錯誤的名稱，應為 act_date
  act_date: string;
  end_date: string;
  // act_date: string;
  img: string;
  organizer: string;
  email: string;
  phone: number;
}

@Injectable({
  providedIn: 'root'
})
export class MayService {
  // Done: start from here 2021/8/24

  private TICKET_URI = 'http://163.17.9.165/ords/app107_17/club/tickets/';

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Fetch a ticket from the database.
   * @param id Ticket id
   */
  public fetchTicket(id: number): Observable<May[] | string> {
    let activities: May [] = [];
    return this.httpClient.get(this.TICKET_URI + id)
      .pipe(
        // tap((response: OrdsResponse<TicketTable>) =>
        // {response.items.forEach(
        //   (value => console.log(value))
        // ); }),
        // tap((value) => console.log('MayService: ', value)),
        map((response: OrdsResponse<MayTable>) => {
          // console.log('fetchAllTickets: ', new Date(response.items[0].end_date));
          // convert OrdsResponse<MayTable> from to Map[].
          // debugger
          activities = response.items.map((may: MayTable) => {
            const mayActivity = new May();
            mayActivity.id = may.id;
            mayActivity.title = may.title;
            mayActivity.startDate = new Date(may.start_date);
            mayActivity.actDate = new Date(may.act_date);
            mayActivity.endDate = new Date(may.end_date);
            mayActivity.img = may.img;
            mayActivity.email = may.email;
            mayActivity.intro = may.intro;
            mayActivity.organizer = may.organizer;
            mayActivity.phone = may.phone;
            mayActivity.venue = may.venue;
            return mayActivity;
          }
          );
          // console.log('After converted to Map[]:', activities);
          return activities;
        }),
        catchError(error => of('Caught: ${error}'))
      );
  }
}


