import { Injectable } from '@angular/core';
import {Activity} from '../models/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  private  activities: Activity [];

  constructor() {
    this.activities = [];
    this.activities.push(
      new Activity('弘光科技大學英雄聯盟杯校內賽',
        new Date(1010, 9, 10),
        new Date(2020, 10, 5),
       '弘光科大電競館地點',
        '弘光科技大學',
        'https://i.imgur.com/OUywY0C.png'),
      new Activity('特戰英豪VALORANT校際盃樹德科大校內賽',
        new Date(2020, 10, 10),
        new Date(2020, 11, 5),
        '樹德科大電競館地點',
        '樹德科技大學',
        'https://i.imgur.com/BPfwD0I.png'),
      new Activity('朝陽科大英雄聯盟校際盃校內賽',
        new Date(2020, 11, 10),
        new Date(2021, 0, 5),
        '朝陽科大電競館地點',
        '朝陽科技大學',
        'https://i.imgur.com/Ikk7vjN.png')
    );
  }

  public findActivities(): Activity [] {

    return this.activities;
  }
}
