import { Component, OnInit } from '@angular/core';
import {KeywordService} from '../../services/keyword.service';
import {Activity} from '../../models/activity';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit {
  // bind 到 input
  searchWords: string;
  // activities: Activity [];

  // Inject keywordService
  // Inject Router to navigate to keywordComponent by codes.
  constructor(private keywordService: KeywordService,
              private router: Router) { }

  ngOnInit(): void {
    this.searchWords = '五';
  }

  // 按下 Search button 時執行此函數
  public search(){
    console.log(this.searchWords);
    // route keywordComponent with the query parameter: searchWords
    this.router.navigate(['/keyword'], {queryParams: {keywords: this.searchWords}});

  }

  onKeyup(event: any) {
    this.searchWords = event.target.value;
    // console.log(this.searchWords);
  }
}
