export interface OrdsResponse<T> {
  items: T [];
  hasMore: boolean;
  limit: number;
  offset: number;
  count: number;
  links: object [];
}
