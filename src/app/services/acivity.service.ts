import { Injectable } from '@angular/core';
import {Activity} from '../models/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  private  activities: Activity [];

  constructor() {
    this.activities = [];
    this.activities.push(
      new Activity('五月天[ 好好好想見到你 ] 演唱會',
        new Date(2021, 2, 28),
        new Date(2021, 3, 10),
        '臺南市立體育場',
        '2020五月天[ 好好好想見到你 ] 跨年演唱會，3個城市桃園 台中 台南15場次帶你飛向2021',
        'https://i.imgur.com/JGG56fB.png'),
      new Activity('特戰英豪VALORANT校際盃樹德科大校內賽',
        new Date(2020, 10, 10),
        new Date(2020, 12, 5),
        '樹德科大電競館地點',
        '樹德科技大學',
        'https://i.imgur.com/BPfwD0I.png'),
      new Activity('朝陽科大英雄聯盟校際盃校內賽',
        new Date(2021, 0, 5),
        new Date(2021, 1, 5),
        '朝陽科大電競館地點',
        '朝陽科技大學',
        'https://i.imgur.com/Ikk7vjN.png'),
      new Activity('朝陽歌唱大會',
        new Date(2025, 4, 30),
        new Date(2025, 5, 30),
        '臺中市霧峰區吉峰東路168號',
        '朝陽歌唱大會在朝陽科大操場舉辦，邀請全校師生一起來觀賞。',
        'https://images.chinatimes.com/newsphoto/2021-05-19/656/20210519002064.jpg')
    );
  }

  public findActivities(): Activity [] {

    return this.activities;
  }
}

