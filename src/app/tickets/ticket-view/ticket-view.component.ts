import { Component, OnInit } from '@angular/core';
import {ActivityService} from '../../services/acivity.service';
import {Activity} from '../../models/activity';
import {TicketService} from '../../services/ticket.service';

@Component({
  selector: 'app-ticket-view',
  templateUrl: './ticket-view.component.html',
  styleUrls: ['./ticket-view.component.css']
})

export class TicketViewComponent implements OnInit {
  activities: Activity[] | string;
  constructor(private ticketService: TicketService) {
  }

  ngOnInit(): void {
    this.ticketService.fetchAllTickets().subscribe(
      (value => this.activities = value),
      ((error => {console.log(error); }))
    );
  }

}
