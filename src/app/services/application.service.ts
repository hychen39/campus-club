import {Injectable} from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import {Application} from '../models/application';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';

/**
 * Post a new activity application to database.
 */


@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  // Done: start from here 2021/8/24
  private APPLICATION_URI = 'http://163.17.9.165/ords/app107_17/test/application';

  constructor(private httpClient: HttpClient) {
  }

  // ToDo
  /**
   * 將申請書儲存到資料庫. 回傳 response status.
   * @param app
   * @return responseStatus
   */
  public submit(app: Application): Observable<string>{
    const appJson = JSON.stringify(app);
    const postUrl = 'http://163.17.9.165/ords/app107_17/club/application';
    return this.httpClient
      .post(postUrl, app, {observe: 'body' as const, responseType: 'json' as const})
      .pipe( map((value: {'organizer': string}) => value.organizer),
        catchError( ((err: HttpErrorResponse) => {
          return throwError(err.error.text as string);
        })));

  }

  /**
   * 產生一個測試的活動申請資料
   */
  public genTestActivityAppData(): Application{
    return new Application(
      1,
      '校內',
      '李同學',
      '0911-113-114',
      'lee@gmail.com',
      '25',
      '設計大樓禮堂',
      new Date('2021/09/04'),
      '大家一起來',
      '大家一起來歌唱',
      '表演內容',
      '人文藝術',
      '演唱會',
      '#',
      1000,
      'tag'
    );
  }
}



