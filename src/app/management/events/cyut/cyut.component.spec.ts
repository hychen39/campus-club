import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CyutComponent } from './cyut.component';

describe('CyutComponent', () => {
  let component: CyutComponent;
  let fixture: ComponentFixture<CyutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CyutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CyutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

