export class Activity {
  public id: number;
  public  title: string;
  public  startDate: Date;
  public  endDate: Date;
  public venue: string; // 場地
  public intro: string; // 活動簡要說明
  public img: string; // image url or base64 string

  constructor(title: string, startDate: Date, endDate: Date, venue: string, intro: string, img: string) {
    this.title = title;
    this.startDate = startDate;
    this.endDate = endDate;
    this.venue = venue;
    this.intro = intro;
    this.img = img;
  }
}

