/**
 * 活動申請
 */
export class Application {
    // tslint:disable-next-line:variable-name
    constructor(public id?: number,
                // tslint:disable:variable-name
                public organizer ?: string,
                public activity_leader ?: string,
                public phone ?: string,
                public email ?: string,
                public people ?: string,
                public venue ?: string,
                public act_date ?: Date,
                public activity_name ?: string,
                public intro ?: string,
                public content ?: string,
                public activity_category ?: string,
                public interest_category ?: string,
                public url ?: string,
                public credit_limit ?: number,
                public tags ?: string
    ){}
}
