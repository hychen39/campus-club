export class May {
    public id: number;
    public title: string;
    public startDate: Date;
    public actDate: Date;
    public endDate: Date;
    public venue: string; // 場地
    public intro: string; // 活動簡要說明
    public img: string; // image url or base64 string
    public organizer: string;
    public email: string;
    public phone: number;

    constructor() {
    }
    // constructor(id: number, title: string, startDate: date, actDate: Date, endDate: Date, venue: string,
    //             intro: string, img: string, organizer: string, email: string, phone: number) {
    //   this.id = id;
    //   this.title = title;
    //   debugger
    //   this.startDate = startDate;
    //   this.actDate = actDate;
    //   this.endDate = endDate;
    //   this.venue = venue;
    //   this.intro = intro;
    //   this.img = img;
    //   this.organizer = organizer;
    //   this.email = email;
    //   this.phone = phone;
    // }
  }
