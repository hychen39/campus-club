import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageHeaderComponent } from './common/page-header/page-header.component';
import { PageFooterComponent } from './common/page-footer/page-footer.component';
import { ActivityViewComponent } from './activities/activity-view/activity-view.component';
import { TicketViewComponent } from './tickets/ticket-view/ticket-view.component';

import { CategoryViewComponent } from './category/category-view/category-view.component';
import { InterestViewComponent } from './interest/interest-view/interest-view.component';
import { MayViewComponent } from './may/may-view/may-view.component';
import { MessageViewComponent } from './message/message-view/message-view.component';
import { ManagementComponent } from './management/management.component';
import { ApplicationComponent } from './management/application/application.component';
import { EventsComponent } from './management/events/events.component';
import { CyutComponent } from './management/events/cyut/cyut.component';
import { SearchComponent } from './management/search/search.component';
import { MayactiComponent } from './management/search/mayacti/mayacti.component';
import { AccComponent } from './management/acc/acc.component';
import { ActreviewComponent } from './management/actreview/actreview.component';
import { ModiComponent } from './management/actreview/modi/modi.component';
import { PublishComponent } from './management/publish/publish.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { KeywordComponent } from './keyword/keyword.component';


@NgModule({
  declarations: [
    AppComponent,
    PageHeaderComponent,
    PageFooterComponent,
    ActivityViewComponent,
    TicketViewComponent,

    CategoryViewComponent,

    InterestViewComponent,

    MayViewComponent,
         MessageViewComponent,
         ManagementComponent,
         ApplicationComponent,
         EventsComponent,
         CyutComponent,
         SearchComponent,
         MayactiComponent,
         AccComponent,
         ActreviewComponent,
         ModiComponent,
         PublishComponent,
         KeywordComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
